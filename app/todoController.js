todoList.controller('todoController', ['$resource', function($resource) {
  var self = this;
  self.items = [];

  self.addItem = function(item) {
    self.items.push(item);
    self.todoItem = "";
  };

  self.deleteItem = function(item) {
    var index = self.items.indexOf(item);
    self.items.splice(index, 1);
  };

  self.clearAll = function() {
    self.items = [];
  };
}]);
