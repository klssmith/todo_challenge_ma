describe('todoController', function() {
  beforeEach(module('todoList'));
  var ctrl;

  beforeEach(inject(function($controller) {
    ctrl = $controller('todoController');
  }));

  it('contains a variable called items', function() {
    expect(ctrl.items).toBeDefined();
  });

  it('can add a todo into the items array', function() {
    ctrl.addItem('Walk the dog');
    expect(ctrl.items).toContain('Walk the dog');
  });

  it('clears the text box when add item is clicked', function() {
    ctrl.addItem('Tidy');
    expect(ctrl.todoItem).toEqual("");
  });

  it('deletes an item when the delete button is clicked', function() {
    ctrl.addItem('Do that thing');
    ctrl.deleteItem('Do that thing');
    expect(ctrl.items).toEqual([]);
  });

  it('can clear all items', function() {
    ctrl.addItem('Do that thing');
    ctrl.addItem('Do thing number two');
    ctrl.clearAll();
    expect(ctrl.items).toEqual([]);
  });
});
