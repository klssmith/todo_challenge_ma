describe('ToDo Challenge', function() {
  beforeEach(function() {
    browser.get('http://localhost:8080');
  });

  var todoBox = element(by.model('todoCtrl.todoItem'));
  var todoButton = element(by.id('addItem'));
  var todoItems = element.all(by.repeater('item in todoCtrl.items'));
  var firstItem = todoItems.get(0).element(by.id('todoItem'));
  var firstTableRow = todoItems.get(0).element(by.id('tableRow'));
  var clearButton = element(by.id('clearItems'));

  it('has a title', function() {
    expect(browser.getTitle()).toEqual('To Do List');
  });

  it('lets the user store tasks', function() {
    todoBox.sendKeys('Do weekend challenge');
    todoButton.click();
    expect(firstItem.getText()).toEqual('Do weekend challenge');
  });

  it('lets the user delete a task', function() {
    todoBox.sendKeys('Do weekend challenge');
    todoButton.click();
    todoBox.sendKeys('Buy milk');
    todoButton.click();
    todoItems.get(0).element(by.className('btn-danger')).click();
    expect(firstItem.getText()).toEqual('Buy milk');
  });

  it('lets the user mark a task as complete', function() {
    todoBox.sendKeys('Do weekend challenge');
    todoButton.click();
    expect(firstTableRow.getAttribute('class')).not.toMatch(/strikethrough/);
    todoItems.get(0).element(by.model('completed')).click();
    expect(firstTableRow.getAttribute('class')).toMatch(/strikethrough/);
  });

//fix this one later...
  it('lets the user clear all tasks', function() {
    todoBox.sendKeys('Do weekend challenge');
    todoBox.sendKeys('Do some work');
    clearButton.click();
    expect(firstItem).not.toEqual(['Do weekend challenge']);
  });
});
